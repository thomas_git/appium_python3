#!/usr/bin/env python
# -*- coding:utf-8 -*-
from testReport.pyh import *
import time
import os
from testData.Global import *
class HtmlReport:
    def __init__(self):
        self.filename = ''                   # 结果文件名
        self.title = "自动化测试"
        self.data = []
        self.temp = 0
    # 生成HTML报告
    def generate_html(self, file):
        num = 0
        page = PyH(self.title)
        page.addJS("jquery-1.11.2.min.js")
        page.addJS("report.js")
        page.addCSS("report.css")
        page << h1(self.title, align='center')
        page << p('测试总耗时：' + str(common.time_took) + "秒")
        page << p('测试用例数：' + str(common.test_sum_title) + '&nbsp' + '成功用例数：' + str(common.test_success_title) +
                  '&nbsp' + '失败用例数：' + str(common.test_failed_title) + '&nbsp' + '出错用例数：' + str(common.test_error_title))
        main = page << div(id='main')
        main << div(cl='main-m') << ul() << li('序号') + li('模块名') + li('测试总结') + div(cl='clear')
        for item in self.data['data']:
            num += 1
            main_m_m_p_ul = main << div(cl='main-m-m') << div(cl='m-parent') << ul(cl='m-p-ul')
            main_m_m_p_ul << li(str(num)) + li(str(item['test_module'])) +li('测试用例总数：'+str(item['test_sum'])+' '+'成功用例数：'+str(item['test_success'])+' '+ '失败用例数：'+str(item['test_failed'])+' '+'错误的用例数：' +str(item['test_error'])) + div(cl='clear')
            main_m_m_p_ul << div(cl='mm-son-p') << ul(cl='p-title') << li('用例ID') + li('用例介绍') + li('用例名称',cl='p-case-p') + li('测试结果') + div(cl='clear')
            for i in range(len(item['info'])):
                main_m_m_p_ul << div(cl='mm-son-p') <<  ul(cl='p-son-title') << li(str(item['info'][i]['test_id'])) +  li(str(item['info'][i]['test_intr'])) +\
                                                                                           li(str(item['info'][i]['test_case']) ,cl='p-case') +li(str(item['info'][i]['test_report'])) + div(cl='clear')
        self._set_result_filename(file)
        page.printOut(self.filename)
    # 设置结果文件名
    def _set_result_filename(self, filename):
        self.filename = filename
        if os.path.isdir(self.filename):
            raise IOError("%s must point to a file" % filename)
        elif '' == self.filename:
            raise IOError('filename can not be empty')
        else:
            parent_path, ext = os.path.splitext(filename)
            tm = time.strftime('%Y%m%d%H%M%S', time.localtime())
            self.filename = parent_path + tm + ext
    # # 统计运行耗时
    # def time_caculate(self,seconds):
    #     self.time_took = "%.2f" %seconds
    #     return self.time_took
t = {'data': [{'test_failed': '3', 'info': [{'test_id': '1001', 'test_intr': '商品列表', 'test_case': 'test_shop_list', 'test_report': '成功'},
                                            {'test_id': '1002', 'test_intr': '详情页面', 'test_case': 'test_detail', 'test_report': '成功'}],
                                            'test_sum': '33', 'test_error': '0', 'test_module': '首页', 'test_success': '30'},
                                            {'test_failed': '1','info': [{'test_id': '1003', 'test_intr': '登陆', 'test_case': 'test_login', 'test_report': '成功'},
                                            {'test_id': '1004', 'test_intr': '注销', 'test_case': 'test_login_out', 'test_report': '成功'}],
                                             'test_sum': '40', 'test_error': '1', 'test_module': '会员中心', 'test_success': '38'}]}

t1 = {'data': [{'test_error ': 0, 'info': [{'test_report': '成功', 'test_case': 'test_check_home', 'test_id': '1001', 'test_intr': '登陆'},
                                           {'test_report': '成功', 'test_case': 'test_home_more_adv', 'test_id': '1002', 'test_intr': '退出'}],
                'test_failed ': 0, 'test_module': '测试testSHI', "test_success": 1, 'test_sum': '33'},
               # 'test_sum': '33', 'test_error': '0', 'test_module': '首页', 'test_success': '30'

               {'test_error ': 0, 'info': [{'test_report': '成功', 'test_case': 'test_check_home', 'test_id': '1001', 'test_intr': '登陆'},
                                           {'test_report': '成功', 'test_case': 'test_home_more_adv', 'test_id': '1002', 'test_intr': '退出'}],
                'test_failed ': 0, 'test_module': '测试chathome', 'test_success ': 1, 'test_sum ': '1'}]}




# html_report1 = HtmlReport()
# html_report1.data = t1
# html_report1.generate_html( r'd:\\app\\appium_study\\testReport\\report.html')     # 生成测试报告

