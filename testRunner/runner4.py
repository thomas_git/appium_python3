__author__ = 'Administrator'
# -*- coding: utf-8 -*-
from testRunner.runner import *
from testCase.myinfo.login import *
from testCase.home.Home import *
from testCase.home.Home1 import *
from testCase.zhishang import daka
from testReport.htmlreport import *
import datetime
from testRunner.testServer import AppiumServer
from testAppBase.AppBaseMsg import apkInfo
from testAppBase.appKernel import *
from testAppBase.adb_common import AndroidDebugBridge
from testReport.BaseExcelReport import *
import xlsxwriter
from testReport import sendMail as sd
from testReport.getEmail import *
def runnerCaseApp():
    start_test_time = time.strftime("%Y-%m-%d %H:%M %p", time.localtime())
    ai = apkInfo(r"D:\app\appium_study\t1.apk")
    apk_name = ai.get_apk_name() # app名字
    apk_siz = ai.get_apk_size() # app大小
    apk_version = ai.get_apk_version() # app版本

    get_phone_msg = getPhoneMsg("d:\phone.txt")

    release = get_phone_msg["release"] # 系统版本
    phone_name = get_phone_msg["phone_name"] +" " +get_phone_msg["phone_model"] # 手机名字

    men_total = get_men_total("d:\men_total.txt") # 运行内存
    cpu_sum = get_cpu_kel("d:\cpu.txt") # 几核cpu

    pix = get_app_pix() # 得到手机分辨率


    suite = unittest.TestSuite()
    starttime = datetime.datetime.now()



    suite.addTest(TestInterfaceCase.parametrize(testHome))
    suite.addTest(TestInterfaceCase.parametrize(testHome1))
    # suite.addTest(TestInterfaceCase.parametrize(testHome))
    unittest.TextTestRunner(verbosity=2).run(suite)
    endtime = datetime.datetime.now()

    common.RRPORT["test_sum"] = common.test_sum
    common.RRPORT["test_failed"] = common.test_failed
    common.RRPORT["test_success"] = common.test_success
    common.RRPORT["test_sum_date"] = (endtime - starttime).seconds
    common.RRPORT["app_name"] = apk_name
    common.RRPORT["app_size"] = apk_siz
    common.RRPORT["phone_name"] = phone_name
    common.RRPORT["phone_rel"] = release
    common.RRPORT["phone_pix"] = pix
    common.RRPORT["phone_raw"] = men_total
    common.RRPORT["phone_avg_use_raw"] = "内存平均使用情况"
    common.RRPORT["phone_max_use_raw"] = "内存最大峰值"
    common.RRPORT["phone_cpu"] = cpu_sum
    common.RRPORT["phone_avg_use_cpu"] = "cpu平均使用情况"
    common.RRPORT["phone_avg_max_use_cpu"] = "cpu最大峰值使用情况"
    common.RRPORT["app_version"] = apk_version
    common.RRPORT["test_date"] = start_test_time



# web 入口
def runnerCaseWeb(runner):
    pass
if __name__ == '__main__':
    if AndroidDebugBridge().attached_devices():
        if ga.platformName == common.ANDROID or ga.platformName == common.IOS:
            appium_server = AppiumServer(ga.appiumJs, ga.Remote)
            appium_server.start_server()
            while not appium_server.is_runnnig():
                time.sleep(1)
            runnerCaseApp()
            # html_report1 = HtmlReport()
            # html_report1.data = common.RRPORT
            # html_report1.generate_html( r'd:\\app\\appium_study\\testReport\\report.html')
            workbook = xlsxwriter.Workbook('report.xlsx')
            worksheet = workbook.add_worksheet("测试总况")
            worksheet2 = workbook.add_worksheet("测试详情")
            print(common.RRPORT)
            bc = xlsxwriterBase(wd=workbook, data=common.RRPORT)
            bc.init(worksheet)
            bc.test_detail(worksheet2)
            bc.close()
            get_email = read_email("D:\\app\\appium_study\\email.ini")
            sd.send_mail(f="report.xlsx", to_addr=get_email[0], mail_host=get_email[1], mail_user=get_email[2], mail_pass=get_email[3], port=int(get_email[4]), headerMsg="自动化测试报告", attach="自动化测试报告")
            appium_server.stop_server()
    else:
        print(u"设备不存在")
