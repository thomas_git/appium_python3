__author__ = 'Administrator'
# -*- coding: utf-8 -*-
from testMode.BaseTestCase import *
from testBaseOperate.BaseGetYam import *
import json
from testBaseOperate.BaseOperateElement import *
from testReport.testLog import *
from testData.Global import *
from testAppBase.AppPerformance import *

# def getNull(gh, key, bt):
#     resutl = ""
#     if gh.get(key, "false") != "false":
#         resutl = bt.key = gh[key]
#     return resutl
temp = getTempCase()

def getModeList(f):
    bs = []
    gh = getYam(f)
    for i in range(len(gh)):
        if i == 0:
              #用例id
            temp.test_id = gh[i].get("test_id", "false")
             # 用例介绍
            temp.test_intr = gh[i].get("test_intr", "false")
        bt = BaseTestCase()
        bt.element_info = gh[i]["element_info"]

      # 操作类型
        bt.operate_type = gh[i].get("operate_type", "false")
        bt.element_type = gh[i]["findElemtType"]
        # 输入文字
        bt.msg = gh[i].get("msg", "false")

       # 验证类型
        bt.find_type =gh[i].get("find_type", "false")
        bs.append(json.loads(json.dumps(bt.to_primitive())))
    print("modeList")
    print(bs)
    return bs



class BexceCase():
    """
    :param f: 读取用例文件位置
    :param kwargs: test_id,test_intr,test_case,isLast
    :return:
    """
    def __init__(self, test_module=""):
        self.test_module = test_module

    def execCase(self, f, **kwargs):

        bc = getModeList(f)
        go = getOperateElement(driver=common.DRIVER)
        ch_check = bc[-1]
        for k in bc:
            if k["operate_type"] != "false":
                go.operate_element(k["operate_type"], k["element_type"], k["element_info"])
                common.MEN.append(get_men(common.PACKAGE))
                common.CPU.append(top_cpu(common.PACKAGE))


        # logTest = myLog.getLog()
        if go.findElement(elemt_by=ch_check["element_type"], element_info=ch_check["element_info"], type=ch_check["find_type"]):
            # logTest.resultOK(self.result["test_module"])
            common.test_success += 1
            temp.test_result = "成功"
        else:
            # logTest.resultNG(self.result["test_module"])
            common.test_failed += 1
            temp.test_result = "失败"
            temp.test_reason = "扩展"


        temp.test_name =kwargs["test_name"]
        temp.test_module = self.test_module
        common.test_sum += 1

        common.RESULT["info"].append(json.loads(json.dumps(temp.to_primitive())))
        if kwargs["isLast"] == "1":
        # 最后case要写最下面的统计步骤
            common.RRPORT["info"].append(common.RESULT["info"])
# getModeList(r"D:\appium\testcase\myinfo\login.yaml")



